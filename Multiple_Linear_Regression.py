from flask import Flask
from sklearn import linear_model
from sklearn.model_selection import train_test_split  
import matplotlib.pyplot as plt
import pandas as pd
import pylab as pl
import numpy as np

app = Flask(__name__)

@app.route('/fertilizer/<crop>')
def fertilizer_crop(crop):
 if crop=="rice":
    fertilize="Green K-liquid and Enriched orga and cowdung "
    return fertilize
 if crop=="wheat":
    fertilize="phosphorus and cowdung and super phosphate and nitro phosphate"
    return fertilize
 if crop =="bean":
    fertilize="nitragen and phosphorus and potassium"
    return fertilize
@app.route('/month/<crop>')
def month_crop(crop):
 if crop=="rice":
    sowing="sowing is Jan-Mar  "
    harvesting="harvesting is Apr-July"
    return sowing +"\n"+ harvesting
 if crop=="wheat":
    sowing="sowing is sep-oct"
    harvesting="harvesting is may-Jun"
    return sowing +"\n"+ harvesting
 if crop =="bean":
    sowing="anytime but it will take 6-8 weeks"
    harvesting="harvest after 6-8 weeks"
    return sowing +"\n"+ harvesting
     
@app.route('/predict/<samples>/<temperature>/<humidity>/<moisture>/<light>')
def predict_temperature(samples,temperature,humidity,moisture,light):

  temp=float(temperature)
  humid=float(humidity)
  mois=float(moisture)
  light=float(light)
  sam=samples+".csv"
  data=pd.read_csv(sam)
  x=np.array(data[['temperature','humidity','moisture','light']])
  y=np.array(data.production)
  reg=linear_model.LinearRegression()
  X_train,X_test,Y_train,y_test=train_test_split(x,y,test_size=0.25)
  reg.fit(x,y)
  print (reg.coef_)
  print ("coefficient for temperature:  ")
  print (reg.coef_[0])
  print ("coefficient for humidity:  ")
  print (reg.coef_[1])
  print ("coefficient for moisture: ")
  print (reg.coef_[2])
  print ("coefficient for light: ")
  print (reg.coef_[3])
  print (reg.intercept_)

  '''temp=float(input("enter the temperature:"))
  humid=float(input("enter the humidity:"))
  mois=float(input("enter the moisture:"))
  light=float(input("enter the light:"))
'''
  #predd=temp*float(reg.coef_[0])+humid*float(reg.coef_[1])+mois*float(reg.coef_[2])+sam*float(reg.coef_[3])+float(reg.intercept_)
  pred=reg.predict([[temp,humid,mois,light]])
  print (reg.coef_[0]*temp+reg.coef_[1]*humid+reg.coef_[2]*mois+reg.coef_[3]*light+reg.intercept_)
  #print (predd)
  print (pred)
  pdd=int(pred)
  predd=str(pdd)+"%"
  return predd

if __name__ == '__main__':
   app.run(host = '192.168.43.156',port=5000,debug = True)  

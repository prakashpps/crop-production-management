package com.example.admin.agriapp;

import android.os.AsyncTask;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private ProgressDialog progressDialog;
    private EditText d1;
    private EditText d2;
    private EditText d3;
    private EditText d4;
    private TextView result;
    private Button ld;
    private Button ft;
    private Spinner plantsamples;


    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        progressDialog = new ProgressDialog(this);


        result = (TextView) findViewById(R.id.res);
        plantsamples = (Spinner) findViewById(R.id.spinner1);
        d1 = (EditText) findViewById(R.id.data);
        d2 = (EditText) findViewById(R.id.data2);
        d3 = (EditText) findViewById(R.id.data3);
        d4 = (EditText) findViewById(R.id.data4);
        ld = (Button) findViewById(R.id.pps);
        ft = (Button) findViewById(R.id.pps2);
        plantsamples.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(MainActivity.this, String.valueOf(plantsamples.getSelectedItem()) + ".csv file selected",
                        Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(MainActivity.this, String.valueOf("select dataset to progress"),
                        Toast.LENGTH_SHORT).show();

            }
        });

        ft.setOnClickListener(this);
        ld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Finding Production efficiency");
                progressDialog.show();
                new Fetchldd().execute();
            }
        });


    }

    private class Fetchldd extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            String pvalue = d1.getText().toString();
            String p1value = d2.getText().toString();
            String p2value = d3.getText().toString();
            String p3value = d4.getText().toString();
            String dataset;
            dataset = String.valueOf(plantsamples.getSelectedItem());


            HttpURLConnection conn = null;
            try {
                String tem = "http://192.168.43.156:5000/predict/"+dataset+"/"+pvalue+"/"+p1value+"/"+p2value+"/"+p3value;
                URL url = new URL(tem);
                conn = (HttpURLConnection) url.openConnection();
                try {

                    InputStreamReader input = new InputStreamReader(conn.getInputStream());
                    int temp = conn.getResponseCode();
                    BufferedReader r = new BufferedReader(input);
                    StringBuilder total = new StringBuilder();
                    for (String line; (line = r.readLine()) != null; ) {
                        total.append(line).append('\n');
                    }
                     result.setText("  Production Efficiency: "+total.toString());
                    progressDialog.dismiss();



                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    conn.disconnect();
                }
            } catch (MalformedURLException e) {
                result.setText("malformed");
            } catch (IOException e) {
                result.setText("Ioexception");
            }
            return null;

        }
    }
    public native String stringFromJNI();

    @Override
    public void onClick(View v) {


        new Fetchdata().execute();

        progressDialog.setMessage("Fetching values");
        progressDialog.show();


    }

    private class Fetchdata extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            HttpURLConnection conn = null;
            try {
                String tem = "http://192.168.43.119/";
                URL url = new URL(tem);
                conn = (HttpURLConnection) url.openConnection();
                try {

                    InputStreamReader input = new InputStreamReader(conn.getInputStream());
                    int temp = conn.getResponseCode();
                    BufferedReader r = new BufferedReader(input);
                    StringBuilder total = new StringBuilder();
                    for (String line; (line = r.readLine()) != null; ) {
                        total.append(line).append('\n');
                    }
                    String stri = total.toString();
                    String[] arrofstr = stri.split("C", 5);
                    d1.setText(arrofstr[0]);
                    d2.setText(arrofstr[1]);
                    d4.setText(arrofstr[2]);
                    progressDialog.dismiss();


                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    conn.disconnect();
                }
            } catch (MalformedURLException e) {
                result.setText("malformed");
            } catch (IOException e) {
                result.setText("Ioexception");
            }
            return null;

        }
    }
}

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <SimpleDHT.h>

const int light = A0;
int lvl;
int pinDHT11 = 2;
SimpleDHT11 dht11;
 
// Replace with your network credentials
const char* ssid = "suresh";
const char* password = "asdfghjkl";
 
ESP8266WebServer server(80);   //instantiate server at port 80 (http port)
 
String page = "";
byte temperature = 0;
byte humidity = 0;
void setup(void){
 
 
  
  delay(1000);
  Serial.begin(115200);
  WiFi.begin(ssid, password); //begin WiFi connection
  Serial.println("");
  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
 
  server.on("/", [](){
    page = String(temperature)+"C"+String(humidity)+"C"+String(lvl);
    server.send(200, "text/html", page);
  });
  
  server.begin();
  Serial.println("Web server started!");
   
}
 
void loop(void){
  
 lvl = analogRead(A0);
   
  if (dht11.read(pinDHT11, &temperature, &humidity,NULL))
  {
    return;
  }
  delay(5000);
  server.handleClient();
  Serial.print(String(temperature));
  Serial.print(String(humidity));
  Serial.print(String(lvl));
  
  
}